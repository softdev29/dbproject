package patchara.databaseproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.databaseproject.helper.DatabaseHelper;
import patchara.databaseproject.model.Product;

public class ProductDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        Product item = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM product WHERE product_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Product.fromRS(rs);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM product";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM product WHERE " + where + " ORDER BY " + order;
//        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM product ORDER BY " + order;
//        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Product save(Product obj) {
//        Connection conn = DatabaseHelper.getConnect();
//        String sql = "﻿INSERT INTO product ()"
//                + "VALUES(?, ?, ?, ?, ?)";
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getPassword());
//            stmt.setInt(4, obj.getRole());
//            stmt.setString(5, obj.getGender());
////            System.out.println(stmt);
//            stmt.executeUpdate();
//            obj.setId(DatabaseHelper.getInsertedId(stmt));
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
        return null;
    }

    @Override
    public Product update(Product obj) {
//        Connection conn = DatabaseHelper.getConnect();
//        String sql = "﻿UPDATE product"
//                + " ﻿SET product_name = ?"
//                + " ﻿WHERE product_id = ?";
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
//            stmt.executeUpdate();
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Product obj) {
//        Connection conn = DatabaseHelper.getConnect();
//        String sql = "DELETE FROM product WHERE product_id=?";
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
//            int ret = stmt.executeUpdate();
//            return ret;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
        return -1;
    }

}
