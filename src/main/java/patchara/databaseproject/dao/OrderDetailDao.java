package patchara.databaseproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.databaseproject.helper.DatabaseHelper;
import patchara.databaseproject.model.OrderDetail;
import patchara.databaseproject.model.OrderDetail;

public class OrderDetailDao implements Dao<OrderDetail> {

    @Override
    public OrderDetail get(int id) {
        OrderDetail item = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM order_detail WHERE orders_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderDetail.fromRS(rs);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<OrderDetail> getAll() {
        ArrayList<OrderDetail> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM order_detail";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<OrderDetail> getAll(String where, String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM order_detail WHERE " + where + " ORDER BY " + order;
//        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<OrderDetail> getByOrdersId(int orderId) {
        return getAll("orders_id = "+orderId," order_detail_id ASC ");
    }

    public List<OrderDetail> getAll(String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM order_detail ORDER BY " + order;
//        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderDetail save(OrderDetail obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿INSERT INTO order_detail (product_id, qty, product_price, product_name, orders_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getProductPrice());
            stmt.setString(4, obj.getProductName());
            stmt.setInt(5, obj.getOrders().getId());
            stmt.executeUpdate();
            obj.setId(DatabaseHelper.getInsertedId(stmt));
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public OrderDetail update(OrderDetail  obj) {
//        Connection conn = DatabaseHelper.getConnect();
//        String sql = "﻿UPDATE order_detail"
//                + " ﻿SET user_login = ?, user_name = ?, user_gender = ?, user_password = ?, user_role = ?"
//                + " ﻿WHERE user_id = ?";
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
//            stmt.executeUpdate();
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(OrderDetail obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM order_detail WHERE orders_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
