package patchara.databaseproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import patchara.databaseproject.helper.DatabaseHelper;
import patchara.databaseproject.model.OrderDetail;
import patchara.databaseproject.model.Orders;

public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        Orders item = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM orders WHERE orders_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Orders.fromRS(rs);
                List<OrderDetail> orderDetails = orderDetailDao.getByOrdersId(item.getId());
                item.setOrderDetail((ArrayList<OrderDetail>) orderDetails);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM orders";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Orders> getAll(String where, String order) {
        ArrayList<Orders> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM orders WHERE " + where + " ORDER BY " + order;

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Orders> getAll(String order) {
        ArrayList<Orders> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM orders ORDER BY " + order;
//        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿INSERT INTO orders (orders_total, orders_qty, orders_date)"
                + "VALUES(?, ?, ?)";
        DatabaseHelper.beginTransaction();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getQty());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getDate()));
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            // add detail
            obj.setId(id);
            for (OrderDetail od : obj.getOrderDetails()) {
                OrderDetail dt = orderDetailDao.save(od);
                if(dt==null){
                    DatabaseHelper.endTransactionWithRollback();
                }
            }
            DatabaseHelper.endTransactionWithCommit();
            return get(id);
        } catch (SQLException ex) {
            DatabaseHelper.endTransactionWithRollback();
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Orders update(Orders obj) {
//        Connection conn = DatabaseHelper.getConnect();
//        String sql = "﻿UPDATE orders"
//                + " ﻿SET user_login = ?, user_name = ?, user_gender = ?, user_password = ?, user_role = ?"
//                + " ﻿WHERE user_id = ?";
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getGender());
//            stmt.setString(4, obj.getPassword());
//            stmt.setInt(5, obj.getRole());
//            stmt.setInt(6, obj.getId());
//            stmt.executeUpdate();
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
        return null;
    }

    @Override
    public int delete(Orders obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM orders WHERE orders_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
