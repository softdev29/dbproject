package patchara.databaseproject.service;

import java.util.List;
import patchara.databaseproject.dao.UserDao;
import patchara.databaseproject.model.User;

public class UserService {

    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll("user_id ASC");
    }

    public User updateUser(User user){
        UserDao userDao = new UserDao();
        return userDao.update(user);
    }
    
    public User newUser(User user){
        UserDao userDao = new UserDao();
        return userDao.save(user);
    }
    
    public int deleteUser(User user){
        UserDao userDao = new UserDao();
        return userDao.delete(user);
    }

    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }
}
