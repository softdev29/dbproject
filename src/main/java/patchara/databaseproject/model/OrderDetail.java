package patchara.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import patchara.databaseproject.dao.ProductDao;

public class OrderDetail {

    private int id;
    Product product;
    private String productName;
    private double productPrice;
    private int qty;
    private Orders orders;

    public OrderDetail(int id, Product product, String productName, double productPrice, int qty, Orders orders) {
        this.id = id;
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.orders = orders;
    }

    public OrderDetail(Product product, String productName, double productPrice, int qty, Orders orders) {
        this.product = product;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.orders = orders;
    }

    public OrderDetail() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", product=" + product + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + '}';
    }

    public double getTotal() {
        return qty * productPrice;
    }

    public static OrderDetail fromRS(ResultSet rs) {
        OrderDetail orderDetail = new OrderDetail();
        ProductDao productDao = new ProductDao();
        try {
            orderDetail.setId(rs.getInt("order_detail_id"));
            int productId = rs.getInt("product_id");
            Product item = productDao.get(productId);
            orderDetail.setProduct(item);
            orderDetail.setQty(rs.getInt("qty"));
            orderDetail.setProductName(rs.getString("product_name"));
            orderDetail.setProductPrice(rs.getDouble("product_price"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return orderDetail;
    }

}
