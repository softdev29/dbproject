
package patchara.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Orders {
    private int id;
    private Date date;
    private int qty;
    private double total;
    private ArrayList<OrderDetail> orderDetails;

    public Orders(int id, Date date, int qty, double total, ArrayList<OrderDetail> orderDetail) {
        this.id = id;
        this.date = date;
        this.qty = qty;
        this.total = total;
        this.orderDetails = orderDetail;
    }
    public Orders() {
        this.id = -1;
        orderDetails = new ArrayList<>();
        this.qty = 0;
        this.total = 0;
        date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double totel) {
        this.total = totel;
    }

    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetail(ArrayList<OrderDetail> orderDetail) {
        this.orderDetails = orderDetail;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", qty=" + qty + ", total=" + total + '}';
    }
    
    public void addOrderDetail(OrderDetail orderDetail){
        orderDetails.add(orderDetail);
        total = total+orderDetail.getTotal();
        qty = qty + orderDetail.getQty();
    }
    
    public void addOrderDetail(Product product, String productName, double productPrice, int qty){
        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
        this.addOrderDetail(orderDetail);
    }
    
    public void addOrderDetail(Product product, int qty){
        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
        this.addOrderDetail(orderDetail);
    }
    
        public static Orders fromRS(ResultSet rs) {
        Orders orders = new Orders();
        try {
            orders.setId(rs.getInt("orders_id"));
            orders.setQty(rs.getInt("orders_qty"));
            orders.setTotal(rs.getInt("orders_total"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            orders.setDate(sdf.parse(rs.getString("orders_date")));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return orders;
    }
}
