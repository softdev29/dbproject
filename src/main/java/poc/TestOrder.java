package poc;

import patchara.databaseproject.dao.OrdersDao;
import patchara.databaseproject.model.Orders;
import patchara.databaseproject.model.Product;
import patchara.databaseproject.model.OrderDetail;

public class TestOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Orders orders = new Orders();

        orders.addOrderDetail(product1, 14);
        orders.addOrderDetail(product2, 21);
        orders.addOrderDetail(product3, 11);
        System.out.println(orders);
        System.out.println(orders.getOrderDetails());
        printReciept(orders);

        OrdersDao ordersDao = new OrdersDao();
        Orders newOrder = ordersDao.save(orders);
        System.out.println(newOrder);

        Orders orders1 = ordersDao.get(newOrder.getId());
        printReciept(orders1);

    }

    static void printReciept(Orders order) {
        System.out.println("Order" + order.getId());
        for (OrderDetail od : order.getOrderDetails()) {
            System.out.println(" " + od.getProductName() + " " + od.getQty() + " " + od.getProductPrice() + " " + od.getTotal());
        }
        System.out.println("Total:" + order.getTotal() + " Qty: " + order.getQty());
    }
}
